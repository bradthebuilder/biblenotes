# Titus
## Introduction
Paul wrote this letter between his first and second imprisonment, after visiting Crete and leaving behind Titus to minister there. The church is plagued with false teachers, and in urgency, Paul writes a short and urgent letter to Titus to guide him.
Key themes include:
* The Gospel by its nature produces godliness in the lives of believers. There is no legitimate separation between belief and behavior.
* One's deeds will either prove or disprove one's claim to know God.
* It is vitally important to have godly men serving as elders and pastors.
* True Christian living will commend the gospel to others.
* Good works have an important place in the lives of believers.
* It is important to deal clearly and firmly with doctrinal and moral error in the church
* The Gospel is the basic for Christian Ethics

## Titus 1:1
This is the only letter where Paul reffers to himself as "a servant of God", as opposed to a "servant of Christ". The intent is to emphasize Paul's place in the lineage of godly men before him, like Moses and David. This is doubled-down on with "an apostle of Jesus Christ". In the context of time and place, when servants spoke, they often invoked the name of their master and spoke with their authority. In this sense, Paul makes his authority abundantly clear at the outset of this letter. "For the sake of the faith of God's elect" next draws attention to the ultimate aim of Paul's ministiry: to preach the Gospel, and in so doing, reach the hearts of men bring them to "knowledge of the truth". "which accords with godliness" sets up a main predicate for the rest of the book; that is, knowledge of the truth, or in other words, living in accordance with the Gospel, produces godliness in act and deed.
## Titus 1:2
"in hope of eternal life" the crux of the gospel. Hope is not wishful thinking, it is a looking forward to a certainty. "God, who never lies". It is a certainty because God promised eternal life, and God cannot lie. God is capable of all, except acting in disagreement with His perfect character. "before the ages began" alludes to God's plan of salvation and its origin predating the world's creation, giving all the more confidence of Jesus' legitimacy as the awaited for Messiah.
## Titus 1:3
"manifested in His word" refers to this promise of eternal life. Taking into account "through the preaching with which I have been entrusted" seems to suggest that Paul is aware of his missionary role including preaching that becomes scripture. "By the command of God our savior" points out that God chose Paul's ministry, not Paul.
## Titus 1:4
Paul refers to Titus in a similar way as he did to Timothy, as a spiritual "child in a common faith". Because both Timothy and Titus had been ordained as church leadership, this title concurs with the context.
## Titus 1:5
Verses 5-16 are summarized as a call to appoint sound elders. This verse is a recap of Titus' mission, as a delegate of Paul, to plant churchs and establish their governance. "In every town as I directed you" suggests that Crete contained many house churches that were entrusted to Titus. Titus has a history with Paul; he was also sent to monitor the church of Corinth. "Appoint" in this verse doesn't mean whimsical picking and choosing of favorites to be elders, it means deliberate selection based off of standardized criteria. "Elders" is plural. A single elder is not sufficient. This situation of Titus receiving authority from Paul to further delegate in the appointing of church elders is explained in Catholicism as informative of a "archbishop overseeing a bishop" network. But that is a mapping of later terminology onto the New Testament; the NT does not mention such a church organization, and the book of Hebrews specifically identifies believers as their own priests, having a direct relationship with Jesus, therefor countering the position of human clergy being required for faith.

Titus was an evangelist. His function was to plant and organize churches. He appointed elders within those churches and proclaimed the Gospel.

The elders sheparded local churches, appointed deacons, trained and appointed other elders, and their authority was limited to only their church.
## Titus 1:6
Qualifications for elders, seen similarly in 1 Timothy.
## Titus 1:7
"Overseer" is used interchangibly with "elder". Paul elaborates on disqualifications of those who would seek this leadership role; in other words to partly address how one becomes "beyond reproach", Paul lists what things one should depart from.
## Titus 1:8
After listing the negative, Paul switches to the positives. These are an elaboration on the summaritive quality of "being beyond reproach". Once again, these align with what is mentioned in 1 Timothy. 
## Titus 1:9
The concluding points on why an elder must possess such lofty qualifications; they will be at war with false teachers; the Judiazers and Gnostics. Of note is the hallmark, distinguishing qualification of an elder as opposed to a deacon, that is, the ability to teach.
## Titus 1:10
Paul uses this transition to remind Titus about the danger of the "circumcision party". These false teachers are in direct contrast to the elders Paul has just described.
## Titus 1:11
Titus and his appointed elders must not give these false teachers a platform to preach their error from. It has apparently already caused some families to stumble. The fact that these false teachers are motivated by greed is a direct contrast to an elder's qualification to "not be greedy for gain".
## Titus 1:12
Paul quotes a famous poem, or hymn to Zeus, of the time that reinforces the charge that Cretans are "liars, evil beasts, lazy gluttons". Crete was known for its moral divergency, with Paul reminds Titus of.
## Titus 1:13
Rather than lambast the Cretan society on its face, Paul agrees with the poet about his charge as a generalization, citing the false teachers as the very confirming sample of the generalization. Given this "rule of thumb", Paul instructs Titus to publicly correct them in their error.
## Titus 1:14
This rebuking is not to "flex" on the false teachers, or to put them in their place, so to speak, but to snap them out of their worldly ways. The goal to preserve them and their true faith, but that comes at the cost of them forgoing their "Jewish myths" and following the words of lieing men over the Word of God.
## Titus 1:15
These next two verses are speaking to the Jewish nature of false teaching that focused on ritual purity (dietary restrictions, the need for circumcision, etc). Paul clearly makes the case countering these notions; God created all things, so all of His creation is good (He observed as much in Genesis). Christians, who are "pure", that is, made pure by Chist's blood, understand this and that all things are pure within the context and constraints that God Himself has outlined. On the other hand, for those who don't understand this, who do not believe in Christ, everything that has the potential for purity is defiled. Money becomes an object of idolatry without the understanding of its place as a tool God gives people to bless others with, for example. Through their actions in this way, the false teachers defile themselves, both in mind and conscience.
## Titus 1:16
The false teachers self-proclaim to know God in a superior way, but such profession is hollow words. They don't have good works to testify to the legitimacy of this claim. On the contrary, their deeds profess the opposite!
## Titus 2:1
Verses 1-8 are similar to what is found in 1 Timothy; a list of qualities for different demographics of the church to abide by. This is the meat of the letter; living by the right doctrine produces the right behavior. This verse begins with Paul setting up by saying that the solution to dealing with these false teachers is to teach sound doctrine.
## Titus 2:2
Measures of proper behavior for older men.
## Titus 2:3
Measures of proper beahvior for older women.
## Titus 2:4
Older women are to train younger women in godly living, including supporting their husbands' leadership role in the household and helping raise the children.
## Titus 2:5
Continued expectations for younger women. Some try to argue that these expectations of godly living for young women form the basis of an argument that all of the expectations mentioned for each demographic are only bound to 1st century culture because they draw the wrong conclusion and take offense at the "working at home" part of this verse. It is not an exclusion of women from work outside the home. It is saying that the domain of the home, its care, and even business that runs from within it, is primarily a woman's entrusted realm, according to God.
## Titus 2:6
Young men are not without the need of practicing self-control.
## Titus 2:7
Paul's instruction in particular to Titus communicating "being the bigger man" and being above reproach in his manner of teaching this doctrine to the church.
## Titus 2:8
We ought to conduct ourselves in such as way as to give an adversary to foothold ot press a charge against us.
## Titus 2:9
Bondservants are submissive to their masters and every man and woman is submissive to their Lord and master, Jesus.
## Titus 2:10
To "adorn the doctrine of God our savior" is to say to live by His doctrine to prove the Gospel's power, truth, and credibility.
## Titus 2:11
This single verse is a cornerstone of Christian theology. God's grace, "appeared" or manifested as "salvation for all people", is the framework of the gospel and New Testament. "The how" and "the who" about God have finally been answered. Jesus is the Lord, and He is full of grace. The Appearance of God's Grace.
## Titus 2:12
The teaching of God's grace is for us to "deny ungodliness and worldly desires". God's grace doesn't only teach us about who He is and how He is, but how we are to be as well. The gnostics were all about the 2 extremes of denial and indulgence; this verse points to the middle-ground that is being a spiritual person in a material world. We deny some things (disobedience to God), and enjoy some material things (His blessings). The Instruction of God's Grace.
## Titus 2:13
Grace is the bedrock of hope in eternal life; Jesus is the master of death (our deaths), so knowing Him to be a grace-filled and loving God, we place our hope in His ability to save us after our deaths. We can wait for Jesus' appearing or our death, at which point we'll see Him. "Waiting" in Greek carries the sense of eagerness. Our wait is an eager one. The Expectation of God's Grace.
## Titus 2:14
Why Grace? This verse. That He might restore us for His pleasure, for us to be used to bless others that he loves. The Purpose of God's Grace.
## Titus 2:15
These last 4 verses are the epitome of what sound doctrine looks like, and thus what Titus should teach. The Authority of God's Grace.
## Titus 3:1
Having established what sound doctrine looks like, Paul goes on to explain what the fruit of sound doctrine being taught looks like. Good Christians are "model Christians" in the light of this verse; they respect authority, are obedient, and stand ready to be of service.
## Titus 3:2
They don't slander or gossip, avoid fighting and extend courtesy.
## Titus 3:3
Chistians are to practice good and godliness because it is the natural, thankful, repentful action from a former life of slavery to different kinds of evil.
## Titus 3:4
In contrast to our former lives, God's goodness presented an alternative way to live. This contrast is because of God and His transformative grace.
## Titus 3:5
Emphasis on what grace means; it's a gift from God, nothing we ever deserved or will deserve. The "washing" hear is a spirtual cleansing by the Holy Spirit, one that is often outwardly expressed through a baptism. Baptism is not the cause for salvation, as this verse (among many others) explains; salvation is initiated by God's grace, not human action or works (baptism).
## Titus 3:6
Jesus is pointed to as "the way", or "the door", or the means through which we receive God's salvation.
## Titus 3:7
By receiving His grace, we become heirs to His promise of eternal life. And this is the gist of the gospel.
## Titus 3:8
"Good deeds" is once again emphasised in this letter. In summation with the preceeding verses, Paul is teaching that Chistians are motivated to 1) not repeat their past, 2) rejoice in their promised salvation, and 3) practice positive reinforcement of that motivation with good works.
## Titus 3:9
Titus is to ignore the "unprofitable" trivial quarrels over genealogies and disputes over The Law.
## Titus 3:10
Moreover, Titus is to ignore the divisive false teachers after giving them fair warning.
## Titus 3:11
Such an individual is ultimately bringing on his own destruction.
## Titus 3:12
The last few verses conclude with personal instructions. Paul asks for Titus to visit him for reasons unknown.
## Titus 3:13
Paul cites two individuals in need of encouraging and supporting 2 converts.
## Titus 3:14
"Our people" probably alludes to those of Jewish ancenstry, in contrast to "Zenas" and "Apollos" in the preceeding verse, which are Greek names. These 2 verses taken together reiterate Paul's claim in Galations 3:28 "neither Jew nor Greek" and combine it with the benefit of good works.
## Titus 3:15
Paul sandwhiches the letter with a salutation from his companions and sends his greetings to those with Titus as Crete. "Grace", the topic of the letter, is the prayer for Titus and the church of Crete.
