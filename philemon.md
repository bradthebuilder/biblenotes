# Philemon
## Introduction
A letter primarily by Paul (and partly by Timothy) to Philemon, a wealthy Chistian in th city of Colassae who owned bondservants. Tychicus and Onesimus were this letter's couriers (Onesimus being the bondservant subject of the letter). The theme of this letter is the power of the Gospel to transform lives and human relationships.

Onesimus fled Philemon, presumably having stolen money or other property, and took refuge in Rome. In so doing, he encountered the apostle Paul during his imprisonment and became a Christian. Paul wrote this letter to appeal to Philemon to receive Onesimus back on the grounds of his transformation in Christ. Now that he has been reconciled to God, he is to be reconciled back to Philemon.

## Philemon 1
Paul reffers to himself as a prisoner of Christ, in reference to his imprisonment in Rome on the cause of preaching Christs' gospel. Timothy is present with Paul, taking care of him. Philemon is addressed as "a beloved fellow worker", which would have been quite the compliment coming from an apostle.
## Philemon 2
Aphhia is Philemon's wife and Archippus and Philemon's son.
## Philemon 3
A greeting of Grace typical of Paul's writings.
## Philemon 4
Verses 4-7 are a prayer on behalf of Philemon; Paul explains his gratitude for Philemon and how he prays for him. 
## Philemon 5
The chief Christian virtue of love is what, understandably, is the source of Paul's gratitude and thanskgiving; love for Christ and His people.
## Philemon 6
"Sharing" of faith includes a spectrum of deeper meaning, including Philemon being a partner of the gospel and its spreading of love to people. This partnership yeilds "full knowledge of every good thing that is in us for the sake of Christ".
## Philemon 7
Philemon's character was excellent; he was known for encouraging fellow believers and ministering to them ina  way that "refreshed" them. For this reason, it is understandable that Paul says that he "derivves much joy" from him and even calls him "brother". This character witness serves as the foundation for his appeal to Philemon latter in the letter.
## Philemon 8
Verses 8-20 constitute Paul's appeal to Philemon. Paul claims confidence in Christ (that is, "What would Jesus do?" in this situation of a fled Onesimus), and confidence in Christ's love being present in Philemon. 
## Philemon 9
Based on this confidence, Paul acknowledges that he could use his apostle authority to give Philemon orders, but instead uses that confidence for his basis of offering an appeal. Paul was around 60 at this time of writing.
## Philemon 10
Paul directly reffers to Onesimus as his son in faith, using literal Greek to compare Onesimus' "spiritual birthing" to the physical pain of child birth.
## Philemon 11
The name "Onesimus" means "useful" or "profitable", so this verse is a fitting play on that knowledge. Although he bore a name of usefulness prior to knowing Christ, he was not in-fact useful until he was reconciled to God. All the more so because Onesimus stole and fled from Philemon; not actions associated with a profitable servant.
## Philemon 12
As Onesimus' spiritual father, Paul has a deep and affectionate love for him; the love which sends him back to Philemon. This is also an explanation of a spirtual perspective on God's sovreignty; Paul us suggesting that God sent Onesimus to him, and now God is instructing him to return him to Philemon.
## Philemon 13
Paul would have enjoyed Onesimus' company during his imprisonment, but he knew that reconcilation to Philemon was a more important matter to attend to.
## Philemon 14
Paul explains that out of his desire for Philemon's love to remain genuine, he refrained from using Onesimus' servitude without his consent. It would not be love if Paul made Philemon act out of "compulsion".
## Philemon 15
A further explanation of God's plan; Onesimus departed for a while such that Philemon may "have him forever", which is a financial jackpot metaphor (slaves were expensive).
## Philemon 16
The basis for that jackpot, to have Onesimus forever, is his new stature in Christ; "a beloved brother". This is the thrust of the appeal; for Onesimus to be received back "no longer as a bondservant". The extra participle "as" underscores the tact that Paul was employing, delivering a softer explanation rather than a decree.
## Philemon 17
A natural extension of both Christ's love and the love of a parent (v12's "my own heart"), Paul asks that Onesimus be received as Paul himself would be received.
## Philemon 18
Paul offers to pay whatever it takes so that Onesimus may be received in love and grace.
## Philemon 19
Paul makes sure to impart on Philemon that he knows that high price of what he is asking. He doesn't ask the favor on Onesimus' behalf blindly. He reiterates that he will pay that balance that is due, forgoing the "bargaining chip" that is Philemon's salvation itself.
## Philemon 20
Paul recounts his openign greeting at this time, seeking refreshment for himself in the good that he is confident that Philemon will do.
## Philemon 21
Paul's conclusion of the letter is not of his appeal, but of his confidence in Philemon.
## Philemon 22
Paul expects, once again, through the sovereignty of God, appealed to through Philemon's prayers, that they will get to see each other soon.
## Philemon 23
Epaphras is a fellow Colssian known to Philemon.
## Philemon 24
A list of fellow apostles and workers with Paul. Of these, Demas turned away from the Gospel at a later time.
## Philemon 25
Another blessing, specifically to Philemon himself.
